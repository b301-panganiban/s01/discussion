package com.zuitt.example;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args){

        Scanner myObj = new Scanner(System.in); // Creating a scanner object
        // Scanner class has various methods to read user input such as nextLine(), nextInt(), nextDouble()

        System.out.println("Enter first name: ");
        String firstName = myObj.nextLine(); // Read user input with the use of the Scanner object

        System.out.println("Enter last name: ");
        String lastName = myObj.nextLine();

        System.out.println("My name is " + firstName + " " + lastName);

    }
}
